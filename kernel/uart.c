//
// low-level driver routines for 16550a UART.
//

#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "spinlock.h"
#include "proc.h"
#include "defs.h"

void
xxuartinit(void)
{
}

// write one output character to the UART.
void
uartputc(int c)
{
	int *uartTx = (int *)0x40008000;
	*uartTx = 0x10000 | c;
}

// read one input character from the UART.
// return -1 if none is waiting.
int
xxuartgetc(void)
{
	return -1;
}

// trap.c calls here when the uart interrupts.
void
uartintr(void)
{
  while(1){
    int c = uartgetc();
    if(c == -1)
      break;
    consoleintr(c);
  }
}

// virtio defs and register for the console output stream:
// - keyboard is channel 0, screen is channel 1
// - use only one buffer (2 is next power of two)
//
#define NUM 2
#include "virtio2.h"

#define R(r) ((volatile uint32 *)(0x4000a000 + (r)))
#define KBD 0

static struct virtq_desc  buf;  
static struct virtq_avail drv; 
static struct virtq_used  dev;

static unsigned char key;

// init console keyboard virtio queue
//
void uartinit(void)
{
	*R(VIRTIO_MMIO_QUEUE_SEL)       = KBD;
	*R(VIRTIO_MMIO_QUEUE_DESC_LOW)  = (uint32) &buf;
	*R(VIRTIO_MMIO_DRIVER_DESC_LOW) = (uint32) &drv;
	*R(VIRTIO_MMIO_DEVICE_DESC_LOW) = (uint32) &dev;
	*R(VIRTIO_MMIO_QUEUE_NUM)       = NUM;

	// set up queue structure with one buffer
	buf.addr    = (unsigned int)&key;
	buf.len     = 1;
	buf.flags   = VRING_DESC_F_WRITE;
	drv.ring[0] = drv.ring[1] = 0;	
	drv.idx++;

	*R(VIRTIO_MMIO_QUEUE_READY) = 0x1;
}

static unsigned int idx_seen;

int uartgetc(void)
{
	unsigned char *kp = &key; // work around prefetch

	*R(VIRTIO_MMIO_INTERRUPT_ACK) = 1;
	if(dev.idx!=idx_seen) {
		idx_seen++;
		drv.idx++;
		return *kp;
	}
	else
		return -1;
}

