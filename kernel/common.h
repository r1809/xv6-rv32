#ifndef __D1_COMMON_H__
#define __D1_COMMON_H__

#include "types.h"

typedef unsigned long long virtual_addr_t;
typedef unsigned int u32_t;

typedef unsigned long long uint64_t;
typedef unsigned int uint32_t;

#define SSTATUS_SIE (1L << 1)  // Supervisor Interrupt Enable
#define SIE_SEIE (1L << 9) // external
#define SIE_STIE (1L << 5) // timer
#define SIE_SSIE (1L << 1) // software

#endif
