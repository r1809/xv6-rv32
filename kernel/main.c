#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "riscv.h"
#include "defs.h"

volatile static int started = 0;

// start() jumps here in supervisor mode on all CPUs.
void
main()
{
  if(cpuid() == 0){
    consoleinit();
    printfinit();
    printf("\n");
    printf("xv6 kernel is booting\n");
    printf("\n");
    kinit();         // physical page allocator
    printf("[ 1.0 page allocator up]\n");
    kvminit();       // create kernel page table
    printf("[ 2.0 kernel page table set up]\n");
    kvminithart();   // turn on paging
    printf("[ 3.0 paging switched on]\n");
    procinit();      // process table
    printf("[ 4.0 process table set up]\n");
    trapinit();      // trap vectors
    printf("[ 5.0 traps initialised]\n");
    trapinithart();  // install kernel trap vector
    printf("[ 6.0 trap vector installed]\n");
    plicinit();      // set up interrupt controller
    printf("[ 7.0 plic initialised]\n");
    plicinithart();  // ask PLIC for device interrupts
    printf("[ 8.0 plic interrupts enabled]\n");
    binit();         // buffer cache
    printf("[ 9.0 buffer cache set up]\n");
    iinit();         // inode cache
    printf("[10.0 inode cache set up]\n");
    fileinit();      // file table
    printf("[11.0 file table set up]\n");
    virtio_disk_init(); // emulated hard disk
    printf("[11.5 disk init done]\n");
    userinit();      // first user process
    printf("[12.0 process 0 set up]\n");
    __sync_synchronize();
    started = 1;
  } else {
    while(started == 0)
      ;
    __sync_synchronize();
    printf("hart %d starting\n", cpuid());
    kvminithart();    // turn on paging
    trapinithart();   // install kernel trap vector
    plicinithart();   // ask PLIC for device interrupts
  }

  printf("[13.0 enter scheduling loop]\n");
  scheduler();        
}
