#include "common.h"


// enable device interrupts
void
intr_on()
{
	w_sie(r_sie() | SIE_SEIE | SIE_STIE | SIE_SSIE);
	w_sstatus(r_sstatus() | SSTATUS_SIE);
}

// disable device interrupts
void
intr_off()
{
	w_sstatus(r_sstatus() & ~SSTATUS_SIE);
}

// are device interrupts enabled?
int
intr_get()
{
	uint64 x = r_sstatus();
	return (x & SSTATUS_SIE) != 0;
}
