// Context switch
//
//   void swtch(struct context *old, struct context *new);
//
// Save current registers in old. Load from new.	


TEXT swtch(SB),0,$0
	MOV	R1,0(R8)
	MOV	R2,4(R8)
	//MOV	R3,8(R8)
	MOV	R4,12(R8)
	MOV	R5,16(R8)
	MOV	R6,20(R8)
	MOV	R7,24(R8)

	MOV	8(R2),R9
	MOV	0(R9),R1
	MOV	4(R9),R2
	//MOV	8(R9),R3
	MOV	12(R9),R4
	MOV	16(R9),R5
	MOV	20(R9),R6
	MOV	24(R9),R7
	
	RET
