
#define PMP_R (1L << 0)
#define PMP_W (1L << 1)
#define PMP_X (1L << 2)
#define PMP_MATCH_NAPOT (3L << 3)

uint32 r_plicbase();
void w_pnr(uint32 x);
uint32 r_mtime();

// write to the first 8 PMP config registers

uint32 r_pmpcfg0();
void w_pmpcfg0(uint32 x);
uint32 r_pmpaddr0();
void w_pmpaddr0(uint32 x);
uint32 r_mhartid();

// Machine Status Register, mstatus

#define MSTATUS_MPP_MASK (3L << 11) // previous mode.
#define MSTATUS_MPP_M (3L << 11)
#define MSTATUS_MPP_S (1L << 11)
#define MSTATUS_MPP_U (0L << 11)
#define MSTATUS_MIE (1L << 3)    // machine-mode interrupt enable.

uint32 r_mstatus();
void w_mstatus(uint32 x);

// machine exception program counter, holds the
// instruction address to which a return from
// exception will go.
void w_mepc(uint32 x);

// Supervisor Status Register, sstatus

#define SSTATUS_SPP (1L << 8)  // Previous mode, 1=Supervisor, 0=User
#define SSTATUS_SPIE (1L << 5) // Supervisor Previous Interrupt Enable
#define SSTATUS_UPIE (1L << 4) // User Previous Interrupt Enable
#define SSTATUS_SIE (1L << 1)  // Supervisor Interrupt Enable
#define SSTATUS_UIE (1L << 0)  // User Interrupt Enable

uint32 r_sstatus();
void w_sstatus(uint32 x);

// Supervisor Interrupt Pending
uint32 r_sip();
void w_sip(uint32 x);

// Supervisor Interrupt Enable
#define SIE_SEIE (1L << 9) // external
#define SIE_STIE (1L << 5) // timer
#define SIE_SSIE (1L << 1) // software

uint32 r_sie();
void w_sie(uint32 x);

// Machine-mode Interrupt Enable
#define MIE_MEIE (1L << 11) // external
#define MIE_MTIE (1L << 7)  // timer
#define MIE_MSIE (1L << 3)  // software

uint32 r_mie();
void w_mie(uint32 x);

// machine exception program counter, holds the
// instruction address to which a return from
// exception will go.

void w_sepc(uint32 x);
uint32 r_sepc();

// Machine Exception Delegation
uint32 r_medeleg();
void w_medeleg(uint32 x);

// Machine Interrupt Delegation
uint32 r_mideleg();
void w_mideleg(uint32 x);

// Supervisor Trap-Vector Base Address
// low two bits are mode.
void w_stvec(uint32 x);
uint32 r_stvec();

// Machine-mode interrupt vector
void w_mtvec(uint32 x);

// use riscv's sv39 page table scheme.
// use riscv's sv32 page table scheme.
#define SATP_SV32 (1L << 31)
#define MAKE_SATP(pagetable) (SATP_SV32 | (((uint32)pagetable) >> 12)) // 32 bit

// supervisor address translation and protection;
// holds the address of the page table.
void w_satp(uint32 x);
uint32 r_satp();

// Supervisor Scratch register, for early trap handler in trampoline.S.
void w_sscratch(uint32 x);
void w_mscratch(uint32 x);

// Supervisor Trap Cause
uint32 r_scause();

// Supervisor Trap Value
uint32 r_stval();

// Machine-mode Counter-Enable
void w_mcounteren(uint32 x);
uint32 r_mcounteren();

uint32 r_time();  // machine-mode cycle counter

void intr_on();   // enable device interrupts
void intr_off();  // disable device interrupts
int intr_get();   // are device interrupts enabled?

// read and write tp, the thread pointer, which holds
// this core's hartid (core number), the index into cpus[].
uint32 r_tp();
void w_tp(uint32 x);
uint32 r_ra();
uint32 r_gp();

// flush the TLB.
void sfence_vma();

#define PGSIZE 4096 // bytes per page
#define PGSHIFT 12  // bits of offset within a page

#define PGROUNDUP(sz)  (((sz)+PGSIZE-1) & ~(PGSIZE-1))
#define PGROUNDDOWN(a) (((a)) & ~(PGSIZE-1))

#define PTE_V (1L << 0) // valid
#define PTE_R (1L << 1)
#define PTE_W (1L << 2)
#define PTE_X (1L << 3)
#define PTE_U (1L << 4) // 1 -> user can access
#define PTE_A (1L << 6)
#define PTE_D (1L << 7)

// shift a physical address to the right place for a PTE.
#define PA2PTE(pa) ((((uint32)pa) >> 12) << 10)

#define PTE2PA(pte) (((pte) >> 10) << 12)

#define PTE_FLAGS(pte) ((pte) & 0x3FF)

// extract the TWO (not three, only on 64 bit) 10-bit page table indices from a virtual address.
#define PXMASK          0x3FF // 10 bits
#define PXSHIFT(level)  (PGSHIFT+(10*(level)))
#define PX(level, va) ((((uint32) (va)) >> PXSHIFT(level)) & PXMASK)

// one beyond the highest possible virtual address.
// MAXVA is actually one bit less than the max allowed by
// Sv39, to avoid having to sign-extend virtual addresses
// that have the high bit set.
//#define MAXVA (1L << (9 + 9 + 9 + 12 - 1))

// HACK FOR 32 bit
#define MAXVA 0xFFFFFFFF

typedef uint32 pte_t;
typedef uint32 *pagetable_t; // 1024 PTEs

