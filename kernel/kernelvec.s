	//
        // interrupts and exceptions while in supervisor
        // mode come here.
        //
        // push all registers, call kerneltrap(), restore, return.
        //

#define SRET		WORD $0x10200073
#define MRET		WORD $0x30200073

TEXT kernelvec(SB),0,$-8
        // make room to save registers.
	SUB	$256,R2,R2

        // save the registers.
        MOV R1,    0(R2)
        MOV R2,    8(R2)
        MOV R3,   16(R2)
        MOV R4,   24(R2)
        MOV R5,   32(R2)
        MOV R6,   40(R2)
        MOV R7,   48(R2)
        MOV R8,   56(R2)
        MOV R9,   64(R2)
        MOV R10,  72(R2)
        MOV R11,  80(R2)
        MOV R12,  88(R2)
        MOV R13,  96(R2)
        MOV R14, 104(R2)
        MOV R15, 112(R2)
        MOV R16, 120(R2)
        MOV R17, 128(R2)
        MOV R18, 136(R2)
        MOV R19, 144(R2)
        MOV R20, 152(R2)
        MOV R21, 160(R2)
        MOV R22, 168(R2)
        MOV R23, 176(R2)
        MOV R24, 184(R2)
        MOV R25, 192(R2)
        MOV R26, 200(R2)
        MOV R27, 208(R2)
        MOV R28, 216(R2)
        MOV R29, 224(R2)
        MOV R30, 232(R2)
        MOV R31, 240(R2)

	// call the C trap handler in trap.c
        JAL	,kerneltrap(SB)

        // restore registers.
	MOV   0(R2), R1
        MOV   8(R2), R2
        MOV  16(R2), R3
        MOV  24(R2), R4
        MOV  32(R2), R5
        MOV  40(R2), R6
        MOV  48(R2), R7
        MOV  56(R2), R8
        MOV  64(R2), R9
        MOV  72(R2), R10
        MOV  80(R2), R11
        MOV  88(R2), R12
        MOV  96(R2), R13
        MOV 104(R2), R14
        MOV 112(R2), R15
        MOV 120(R2), R16
        MOV 128(R2), R17
        MOV 136(R2), R18
        MOV 144(R2), R19
        MOV 152(R2), R20
        MOV 160(R2), R21
        MOV 168(R2), R22
        MOV 176(R2), R23
        MOV 184(R2), R24
        MOV 192(R2), R25
        MOV 200(R2), R26
        MOV 208(R2), R27
        MOV 216(R2), R28
        MOV 224(R2), R29
        MOV 232(R2), R30
        MOV 240(R2), R31

        ADD	$256,R2,R2

        // return to whatever we were doing in the kernel.
        SRET


        //
        // machine-mode timer interrupt.
        //

TEXT timervec(SB),0,$0
        // start.c has set up the memory that mscratch points to:
        // scratch[0,4,8,12] : register save area.
        // scratch[16] : address of CLINT's MTIMECMP register.
        // scratch[20] : desired interval between interrupts.
       
	CSRRW	CSR(0x0340), R8, R8	// swap mscratch
	MOV	R9,   0(R8)
	MOV	R10,  4(R8)
	MOV	R11,  8(R8)
	MOV	R12, 12(R8)

        // schedule the next timer interrupt
        // by adding interval to mtimecmp, based  
	// on current value in the time CSR.
	MOV	16(R8),R9	// CLINT_MTIMECMP
	MOV	20(R8),R10
	MOV	 0(R9),R11	// R12:R11 = timecmp
	MOV	 4(R9),R12
	ADD	R10,R11,R11	// R12:R11 += R10
	SLTU	R10,R11,R10
	ADD	R10,R12,R12
	
	MOV	$-1,R10		// careful write 
	MOV	R10, 4(R9)
	MOV	R11, 0(R9)
	MOV	R12, 4(R9)
	
        // raise a supervisor software interrupt.
	MOV	$2,R9
	MOVW	R9,CSR(0x0344)	// sip
	
	MOV	12(R8), R12
	MOV	 8(R8), R11
	MOV	 4(R8), R10
	MOV	 0(R8), R9
	CSRRW	CSR(0x0340), R8, R8	// swap mscratch

        MRET

